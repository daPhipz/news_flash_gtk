use crate::config::{APP_ID, VERSION};
use glib::object::IsA;
use gtk4::prelude::*;
use gtk4::{Application, License, Window};
use libadwaita::prelude::*;
use libadwaita::AboutWindow;

pub const APP_NAME: &str = "NewsFlash";
pub const COPYRIGHT: &str = "Copyright © 2017-2023 Jan Lukas Gernert";
pub const DESCRIPTION: &str = r#"<b>Follow your favorite blogs &amp; news sites.</b>

NewsFlash is an application designed to complement an already existing web-based RSS reader account.

It combines all the advantages of web based services like syncing across all your devices with everything you expect from a modern desktop application.

<b>Features:</b>

 - Desktop notifications
 - Fast search and filtering
 - Tagging
 - Keyboard shortcuts
 - Offline support
 - and much more

<b>Supported Services:</b>

 - Miniflux
 - local RSS
 - fever
 - Fresh RSS
 - NewsBlur
 - Inoreader
 - feedbin
 - Nextcloud News"#;
pub const WEBSITE: &str = "https://gitlab.com/news_flash/news_flash_gtk";
pub const ISSUE_TRACKER: &str = "https://gitlab.com/news_flash/news_flash_gtk/-/issues";
pub const AUTHORS: &[&str] = &["Jan Lukas Gernert", "Felix Bühler", "Alistair Francis"];
pub const DESIGNERS: &[&str] = &["Jan Lukas Gernert"];
pub const ARTISTS: &[&str] = &["Tobias Bernard"];

pub const RELEASE_NOTES: &str = r#"
<ul>
    <li>Drag and drop support for feeds and categories</li>
    <li>New edit dialogs for feeds, categories and tags</li>
    <li>Allow to move feeds between categories via edit dialog</li>
    <li>Custom mathjax inline delimiter adjustable per feed</li>
    <li>Allow sorting feeds and categories alphabetically</li>
    <li>Fix symbolic icon</li>
    <li>Fix sidebar iteration with "only relevant" enabled</li>
    <li>Fix switch state in settings dialog</li>
    <li>Improve error messages in case of API limit</li>
    <li>Local RSS: improve favicon source selection</li>
    <li>Favicons: avoid unnecessary parallel downloads</li>
    <li>Speedup: use tokio runtime instead of threadpool</li>
    <li>Speedup: (re)create HTTP client only when the network changed</li>
    <li>Offline Mode: detect automatically when offline or online</li>
    <li>Offline Mode: allow updating read/starred status of articles</li>
    <li>Article View: make title unclickable if article doesn't provide an URL</li>
    <li>Option to allow NewsFlash to autostart</li>
    <li>Article List: section by day</li>
    <li>Article List: Update layout</li>
    <li>Article List: automatic updates to reflect more tag and feed changes</li>
    <li>Image dialog</li>
    <li>Video dialog</li>
    <li>Fix embeded youtube videos</li>
    <li>Use adwaita SplitViews for resizing sidebar and article list</li>
    <li>Replace Add popover with dialog</li>
    <li>Extract relevant image from HTML as thumbnail</li>
</ul>
"#;

#[derive(Clone, Debug)]
pub struct NewsFlashAbout;

impl NewsFlashAbout {
    pub fn show<A: IsA<Application> + AdwApplicationExt, W: IsA<Window> + GtkWindowExt>(app: &A, window: &W) {
        AboutWindow::builder()
            .application(app)
            .application_name(APP_NAME)
            .transient_for(window)
            .application_icon(APP_ID)
            .developer_name(AUTHORS[0])
            .developers(AUTHORS)
            .designers(DESIGNERS)
            .artists(ARTISTS)
            .license_type(License::Gpl30)
            .version(VERSION)
            .website(WEBSITE)
            .issue_url(ISSUE_TRACKER)
            .comments(DESCRIPTION)
            .copyright(COPYRIGHT)
            .release_notes(RELEASE_NOTES)
            .release_notes_version("3.0")
            .build()
            .present()
    }
}
