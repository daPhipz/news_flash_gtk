use crate::app::App;
use crate::i18n::i18n;
use crate::util::ClockFormat;
use chrono::{Duration, Local, NaiveDateTime, TimeZone};

pub struct DateUtil;

impl DateUtil {
    pub fn format_time(naive_utc: &NaiveDateTime) -> String {
        let local_datetime = Local.from_utc_datetime(naive_utc);
        let clock_format = App::default().desktop_settings().clock_format();

        if clock_format == ClockFormat::F12H {
            format!("{}", local_datetime.format("%I:%M %p"))
        } else {
            format!("{}", local_datetime.format("%k:%M"))
        }
    }

    pub fn format_date(naive_utc: &NaiveDateTime) -> String {
        let local_date = Local.from_utc_datetime(naive_utc).date_naive();
        let now = Local::now().naive_local().date();

        if now == local_date {
            i18n("Today")
        } else if now - local_date == Duration::days(1) {
            i18n("Yesterday")
        } else {
            format!("{}", local_date.format("%e.%m.%Y"))
        }
    }
}
